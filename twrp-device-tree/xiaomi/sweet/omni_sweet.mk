#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from sweet device
$(call inherit-product, device/xiaomi/sweet/device.mk)

PRODUCT_DEVICE := sweet
PRODUCT_NAME := omni_sweet
PRODUCT_BRAND := Redmi
PRODUCT_MODEL := 2209116AG
PRODUCT_MANUFACTURER := xiaomi

PRODUCT_GMS_CLIENTID_BASE := android-xiaomi

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sweet_id2-user 11 RKQ1.210614.002 V816.0.8.0.THGIDXM release-keys"

BUILD_FINGERPRINT := Redmi/sweet_id2/sweet:11/RKQ1.210614.002/V816.0.8.0.THGIDXM:user/release-keys
